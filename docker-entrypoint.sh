#!/bin/sh

set -e

cat << EOF > /rsync.sh
set -e
flock -xn /var/lock/rsync.lck rsync ${RSYNC_OPTIONS} 2>&1
EOF
chmod +x /rsync.sh

cat << EOF > /var/spool/cron/crontabs/root
${RSYNC_CRONTAB} sh /rsync.sh
EOF

exec "$@"
