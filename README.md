# Docker rsync cron job

To build:

```
docker build -t rsync-client .
```

To test:

```
docker run --rm rsync-client
```

To archive a directory to remote server every 5 minutes using rsync:

```
docker run --rm \
 -v /host/path:/container/path \
 -e RSYNC_CRONTAB="*/5 * * * *" \
 -e RSYNC_OPTIONS="-avz /container/path rsync://remote.host:873/module" \
 rsync-client
```

To pull:

```
docker pull registry.mesilat.com/slava/rsync-client
```
