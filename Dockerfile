FROM alpine

ADD docker-entrypoint.sh /

RUN apk add --no-cache tini rsync

ENV \
  RSYNC_CRONTAB="* * * * *" \
  RSYNC_OPTIONS="--version"

ENTRYPOINT [ "/sbin/tini", "--", "/docker-entrypoint.sh" ]

CMD [ "crond", "-f", "-l", "0" ]
